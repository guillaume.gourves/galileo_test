CC = g++
CFLAGS = -Wall
EXEC_NAME = executable
SOURCE_DIR = ./Source
HEADER_DIR = ./Header

# Dossier a include dans votre projet 
INCLUDES = 	-I$(SOURCE_DIR) \
		-I$(HEADER_DIR)

# Librairie a ajouter a votre projet
LIBS = -lmraa

# Placez les fichiers que vous souhaitez compiler dans votre projet. 
OBJ_FILES = 	$(SOURCE_DIR)/main.o \
		$(SOURCE_DIR)/appli.o \
		$(SOURCE_DIR)/i2c.o

# Commande execute lorsque que vous faite un "make ou make all"
all : $(EXEC_NAME)

# Commande execute lorsque que vous faite un "make clean"
clean :
	rm -rf  $(EXEC_NAME) $(OBJ_FILES)
	
.PHONY: clean

$(EXEC_NAME) : $(OBJ_FILES)
	$(CC) -o $(EXEC_NAME) $(OBJ_FILES) $(LIBS)

%.o: %.cpp
	$(CC) $(CFLAGS) $(INCLUDES) -o $@ -c $<

%.o: %.cc
	$(CC) $(CFLAGS) $(INCLUDES) -o $@ -c $<

%.o: %.c
	gcc $(CFLAGS) $(INCLUDES) -o $@ -c $<
