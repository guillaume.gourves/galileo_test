/**
* @file 	appli.hpp
* @date 	JJ/MM/AAAA
* @author 	Ingenieur X
* @brief 	Header of ---
*/

#ifndef APPLI_HPP_
#define APPLI_HPP_

/**
* @fn bool appli_run(int var)
* @param var 
* @return Return a flag which indicate if the function work (true : yes, false : no).
* @brief Run function 
*/
bool appli_run(int var);

#endif 
