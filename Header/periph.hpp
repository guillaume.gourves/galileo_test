/**
* @file periph.hpp
* @date	JJ/MM/YYYY
* @author Ingenieur X
* @brief 
*/

#ifndef PERIPH_HPP_
#define  PERIPH_HPP_

/**
* @class Periph
* @brief Description
*/
 
class Periph
{
public :

	/**
	* @fn Periph()
	* @brief Constructeur par défaut
	*/
	Periph()
	{
		;
	}
	
	/**
	* @fn ~Periph()
	* @brief Destructeur
	*/
	~Periph()
	{
		;
	}	
	
	virtual void Afficher_caracteristique_peripherique(void) = 0;
};

#endif
