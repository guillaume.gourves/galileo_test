/**
* @file i2c.hpp
* @date	JJ/MM/YYYY
* @author Ingenieur X
* @brief Header
*/

#ifndef I2C_HPP_
#define  I2C_HPP_

#include <iostream>
#include "periph.hpp"

/**
* @class my_i2c
* @brief Description
*/
 
class my_i2c
{
private :
	int adresse_i2c;

public :

	/**
	* @fn my_i2c()
	* @brief Constructeur par défaut
	*/
	my_i2c()
	{
		;
	}
	
	/**
	* @fn ~my_i2c()
	* @brief Destructeur
	*/
	~my_i2c()
	{
		;
	}	
	
	/**
	*@fn virtual void Afficher_caracteristique_peripherique(void)
	* @brief Affichage des caracteristiques
	*/
	virtual void Afficher_caracteristique_peripherique(void) 
	{
		std::cout << "Peripherique de type :\t I2C" << std::endl;
	}
	
	void set_adress_i2c(int _adresse_i2c);
	
};

#endif
