Connexion Carte Galliléo via Wifi
====

# Installation

## Matériel :

Concernant ces cartes, vous dervrez posséder deux antennes avec une
connectique u.fl. Connectez celles-ci à votre module wifi, puis alimentez votre
carte Galiléo (Attention : ce type de connectique se déclipse facilement, manipuler
votre carte avec précaution).

## Logiciel

Sur le terminal de vore carte faire :

    connmanctl
    connmanctl > enable wifi
    connmanctl > scan wifi
    connmanctl > services  
    --> repérez le réseau IOT et copier le wifi_*_psk
    connmanctl > agent on
    connmanctl > connect wifi_*_psk
    --> entrez la clef WPA    
    connmanctl > quit

# Connexion

Afin de faciliter l'interfaçage entre votre ordinateur et votre carte Galliléo,
je vous propose de vous connecter via une interface ssh.

Pour cela, effectuez la commande suivante sur le terminal de votre ordinateur :

    ssh root@<IP de votre carte>

Dans le cas où vous souhaitez transférer des fichier de votre PC vers votre
carte, effectuez la commande suivante :

    scp <Chemin fichier>/<Fichier> root@<IP de votre carte>:<Dossier de destination>

Dans le cas où vous souhaitez transférer un dossier, faire :

    scp -r <Chemin dossier>/<Dossier> root@<IP de votre carte>:<Dossier de destination>
