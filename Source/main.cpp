#include "stdio.h"
#include <iostream>
#include "appli.hpp"
#include "i2c.hpp"

using namespace std;
int main (void)
{
	my_i2c periph_i2c;
	periph_i2c.set_adress_i2c(0x8000);
	cout << "Hello World! " << appli_run(1) << endl;
	return 0;
}
